import logging
import time
import re
import sys
import sqlite3
sys.path.append('../../.')

from aiogram import Bot, Dispatcher, executor, types
from config.settings import TOKEN

MESSAGE = 'Привет, {}! Чтобы начать работу с ботом, отправь мне ' \
          'cообщение /set_phone *твой номер телефона*. Пока я умею работать ' \
          'только с номерами из РФ. Пока ты не отправишь мне свой номер ' \
          'телефона, остальные мои функции будут выключены :)' \
          ''
pattern = r'^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$'

logging.basicConfig(level=logging.INFO)
bot = Bot(token=TOKEN)
dp = Dispatcher(bot=bot)


@dp.message_handler(commands=['start'])
async def start_handler(message: types.Message):
    user_name = message.from_user.username
    logging.info(f'@{user_name} started {time.asctime()}')
    await message.reply(MESSAGE.format(user_name))


@dp.message_handler(commands=['set_phone'])
async def set_phone_handler(message: types.Message):
    user_id = message.from_user.id
    user_name = message.from_user.username
    if find_bitch(user_id):
        await message.reply(f'Ты уже отправил мне свой номер телефона')
        logging.info(f'@{user_name} tried to send his phone number again')
        return
    text = message.text
    phone_number = text.split()[1]
    if re.match(pattern, phone_number):
        user_data = (user_id, user_name, phone_number)
        shit_for_db(user_data)
        await message.reply(f'Записал твой номер телефона {phone_number}')
        logging.info(f'@{user_name} added phone number {phone_number} '
                     f'{time.asctime()}')
    else:
        await message.reply(f'Я жду от тебя номер телефона, '
                            f'например 88005553535. Попробуй еще раз')
        logging.info(f'@{user_name} added incorrect phone number '
                     f'{phone_number}'
                     f'{time.asctime()}')


@dp.message_handler(commands=['me'])
async def send_info(message: types.Message):
    user_name = message.from_user.username
    user_id = message.from_user.id
    if not find_bitch(user_id):
        await message.reply('Сначала отправь мне свой номер телефона')
        logging.info(f'@{user_name} tried get info')
        return
    data = get_info(user_id)
    await message.reply(f'Твой ник: {data[0][1]}, '
                        f'номер телефона: {data[0][2]}')
    logging.info(f'@{user_name} got info')


def shit_for_db(user_data):
    db = sqlite3.connect(
        'C:\\Users\\Данил\\Desktop\\template-for-course\\src\\db.sqlite3')
    cursor = db.cursor()
    cursor.execute("INSERT INTO app_person VALUES(?, ?, ?)", user_data)
    db.commit()
    db.close()


def find_bitch(user_id):
    db = sqlite3.connect(
        'C:\\Users\\Данил\\Desktop\\template-for-course\\src\\db.sqlite3')
    cursor = db.cursor()
    cursor.execute("SELECT * FROM app_person WHERE id = ?;", (user_id, ))
    count = len(cursor.fetchall())
    return count != 0


def get_info(user_id):
    db = sqlite3.connect(
        'C:\\Users\\Данил\\Desktop\\template-for-course\\src\\db.sqlite3')
    cursor = db.cursor()
    cursor.execute("SELECT * FROM app_person WHERE id = ?;", (user_id,))
    data = cursor.fetchall()
    return data


if __name__ == '__main__':
    executor.start_polling(dp)
