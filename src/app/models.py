from app.internal.models.admin_user import AdminUser

from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=256)
    phone_number = models.CharField(max_length=256)

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Person'
        verbose_name_plural = 'People'
